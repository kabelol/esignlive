package com.eightbitplatoon.com.esignature.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.eightbitplatoon.com.esignature.utils.PropertiesFile;
import com.silanis.esl.sdk.EslClient;
import com.silanis.esl.sdk.EventNotificationConfig;
import com.silanis.esl.sdk.NotificationEvent;
import com.silanis.esl.sdk.builder.EventNotificationConfigBuilder;

@Path("notfication")
public class ESignNotificationService {

	@GET
	@Path("register/{packageId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, String> registerForEventNotification(@PathParam("packageId") String packageId) throws IOException {
		Properties properties = PropertiesFile.getProperties();
		String apiKey = properties.getProperty("api.key");
		String apiUrl = properties.getProperty("api.url");
		String callbackUrl = properties.getProperty("callback.url");
		String callbackAuthKey = properties.getProperty("callback.auth.key");
		return registerForEventNotification(apiKey, apiUrl, packageId, callbackUrl, callbackAuthKey);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, String> signingProcessCompleteCallback(String postBack) {
		System.out.println(postBack);
		Map<String, String> returnMap = new HashMap<>();
		returnMap.put("response", "success yippee!");
		return returnMap;
	}

	private Map<String, String> registerForEventNotification(String apiKey, String apiUrl, String packageId, String callbackUrl,
			String callbackAuthKey) throws IOException {
		EslClient eslClient = new EslClient(apiKey, apiUrl);
		eslClient.getEventNotificationService()
				.register(EventNotificationConfigBuilder.newEventNotificationConfig(callbackUrl)
						.withKey(callbackAuthKey).forEvent(NotificationEvent.PACKAGE_COMPLETE));
		EventNotificationConfig eventNotificationConfig = eslClient.getEventNotificationService()
				.getEventNotificationConfig();

		Map<String, String> returnMap = new HashMap<>();
		returnMap.put("callback.url", eventNotificationConfig.getUrl());
		returnMap.put("callback.auth.key", eventNotificationConfig.getKey());
		return returnMap;
	}

}
