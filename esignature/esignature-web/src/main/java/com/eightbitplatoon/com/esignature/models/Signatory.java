package com.eightbitplatoon.com.esignature.models;

public class Signatory {

	private String email;
	private String name;
	private String surname;
	private int signingOrder = 1; // the lower the number the higher the priority
	
	public Signatory() {
	}
	
	public Signatory(String email, String name, String surname, int signingOrder) {
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.signingOrder = signingOrder;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getSigningOrder() {
		return signingOrder;
	}
	public void setSigningOrder(int signingOrder) {
		this.signingOrder = signingOrder;
	}
}
