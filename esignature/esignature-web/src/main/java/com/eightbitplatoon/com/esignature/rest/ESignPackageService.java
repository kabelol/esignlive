package com.eightbitplatoon.com.esignature.rest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.eightbitplatoon.com.esignature.models.Signatory;
import com.eightbitplatoon.com.esignature.utils.PropertiesFile;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.silanis.esl.sdk.DocumentPackage;
import com.silanis.esl.sdk.DocumentType;
import com.silanis.esl.sdk.EslClient;
import com.silanis.esl.sdk.PackageId;
import com.silanis.esl.sdk.builder.DocumentBuilder;
import com.silanis.esl.sdk.builder.PackageBuilder;
import com.silanis.esl.sdk.builder.SignatureBuilder;
import com.silanis.esl.sdk.builder.SignerBuilder;

@Path("package")
public class ESignPackageService {

	@Context
	private ServletContext context;

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, String> creatPackage(MultipartFormDataInput input) throws IOException, URISyntaxException {
		Properties properties = PropertiesFile.getProperties();
		String apiKey = properties.getProperty("api.key");
		String apiUrl = properties.getProperty("api.url");

		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode json = objectMapper.readTree(input.getFormDataMap().get("esignLivePackage").get(0).getBodyAsString());

		List<Signatory> signatories = objectMapper.readValue(json.findValue("signatories").toString(),
				objectMapper.getTypeFactory().constructCollectionType(List.class, Signatory.class));
		String packageName = json.findValue("packageName").asText();
		String documentName = json.findValue("packageDocument").asText();
		String packageId = null;
		try (InputStream inputStream = input.getFormDataMap().get("packageDocument").get(0).getBody(InputStream.class,
				null)) {
			if (DocumentType.PDF.toString().equalsIgnoreCase(
					documentName.substring(documentName.lastIndexOf('.') + 1, documentName.length()))) {
				packageId = createAndSendPackage(apiKey, apiUrl, packageName, signatories, documentName, inputStream,
						DocumentType.PDF);
			}
		}

		Map<String, String> returnMap = new HashMap<>();
		returnMap.put("packageId", packageId);
		return returnMap;
	}

	private String createAndSendPackage(String apiKey, String apiUrl, String packageName, List<Signatory> signatories,
			String documentName, InputStream inputStream, DocumentType documentType) {
		EslClient eslClient = new EslClient(apiKey, apiUrl);
		// Build the DocumentPackage object
		PackageBuilder packageBuilder = PackageBuilder.newPackageNamed(packageName);
		// Add signatories to package builder
		for (Signatory signatory : signatories) {
			packageBuilder.withSigner(
					SignerBuilder.newSignerWithEmail(signatory.getEmail()).withFirstName(signatory.getName())
							.withLastName(signatory.getSurname()).signingOrder(signatory.getSigningOrder()));
		}
		// Select pdf file to use as document
		DocumentBuilder documentBuilder = DocumentBuilder.newDocumentWithName(documentName).fromStream(inputStream,
				documentType);
		// Set position of where each signatories signature should appear
		for (int i = 0; i < signatories.size(); i++) {
			documentBuilder.withSignature(
					SignatureBuilder.signatureFor(signatories.get(i).getEmail()).onPage(0).atPosition(20, i * 100));
		}
		// Build document package
		DocumentPackage documentPackage = packageBuilder.withDocument(documentBuilder).build();
		// Issue the request to the e-SignLive server to create the
		// DocumentPackage
		PackageId packageId = eslClient.createPackage(documentPackage);
		// Send the package to be signed by the participants
		eslClient.sendPackage(packageId);

		return packageId.toString();
	}
}
