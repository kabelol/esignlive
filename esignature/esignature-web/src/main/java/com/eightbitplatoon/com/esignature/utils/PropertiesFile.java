package com.eightbitplatoon.com.esignature.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFile {

	private static Properties properties;

	public static Properties getProperties() throws IOException {
		if (properties == null) {
			try (InputStream inputStream = PropertiesFile.class.getResourceAsStream("/esignlive.properties")) {
				properties = new Properties();
				properties.load(inputStream);
			}
		}
		return properties;
	}
}
